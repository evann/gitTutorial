﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayMessage : MonoBehaviour {

    public string message;
    private bool displayMesage;
    public GameObject player;

	// Use this for initialization
	void Start () {
        message = "Evan your beard looks awful.  You definitely should shave it off.";
        displayMesage = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.M))
        {
            displayMesage = true;
        }
        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            displayMesage = false;
        }
		
	}

    void OnGUI()
    {
        if (displayMesage)
        {
            GUI.Box(new Rect(200, 100, 1000, 100), message);
        }
    }
}
